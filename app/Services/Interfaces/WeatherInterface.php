<?php

namespace App\Services\Interfaces;


interface WeatherInterface
{

    public function currentByCity(string $city): array;

}