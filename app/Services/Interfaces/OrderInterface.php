<?php

namespace App\Services\Interfaces;

use App\Models\Order as OrderAR;

interface OrderInterface
{

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function store(int $id, array $params): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function completed(int $id): bool;

    /**
     * @param int $orderId
     * @return OrderAR
     */
    public function getById(int $orderId): OrderAR;

    /**
     * @return OrderAR
     */
    public function getAll();
}