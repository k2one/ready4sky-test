<?php

namespace App\Services\Weather;

use GuzzleHttp\Client;
use App\Exceptions\ErrorGetWeatherException;
use App\Services\Interfaces\WeatherInterface;

/**
 * Class Weather
 */
class OpenWeather implements WeatherInterface
{
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $city
     * @return array
     * @throws ErrorGetWeatherException
     */
    public function currentByCity(string $city): array
    {
        try {
            $client = new Client([
                'base_uri' => 'api.openweathermap.org/data/2.5/weather?q=' . $city . ',ru&units=metric&lang=ru&APPID=' . $this->apiKey,
                'timeout'  => 2.0,
            ]);

            $response = $client->request('GET');
            $resRaw = json_decode($response->getBody()->getContents());

            $res['temp'] = $resRaw->main->temp;
            $res['description'] = $resRaw->weather[0]->description;
            $res['pressure'] = $resRaw->main->pressure;
            $res['humidity'] = $resRaw->main->humidity;
            $res['wind_speed'] = $resRaw->wind->speed;
            $res['temp_min'] = $resRaw->main->temp_min;
            $res['temp_max'] = $resRaw->main->temp_max;

            return $res;
        } catch (\Throwable $e) {
            throw new ErrorGetWeatherException();
        }
    }

}