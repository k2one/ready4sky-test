<?php

namespace App\Services;

use App\Models\Order as OrderAR;
use App\Services\Interfaces\OrderInterface;
use App\Models\Vendor;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailOrderDone;

class Order implements OrderInterface
{

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function store(int $id, array $params): bool
    {
        $Order = OrderAR::find($id);

        // Должно же быть что то лучше?
        if(!empty($params['client_email'])) {
            if(is_string($params['client_email'])) {
                $Order->client_email = $params['client_email'];
            }
        }
        if(!empty($params['partner_id'])) {
            if(is_string($params['partner_id']) || is_int($params['partner_id'])) {
                $Order->partner_id = (int)$params['partner_id'];
            }
        }
        if(!empty($params['status'])) {
            if(is_string($params['status']) || is_int($params['status'])) {
                $Order->status = (int)$params['status'];
                #if($params['status'] == 20) { // тут бы распараллелить :)
                #    $this->completed($id);
                #}
            }
        }

        $Order->save();

        if (!empty($params['status']) && $params['status'] == 20) {
            $this->completed($id);
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function completed(int $id): bool
    {
        $order = $this->getById($id);

        // Разошлем письма - партнеру и всем поставщикам продуктов из заказа
        $emails[] = $order->partner->email;

        foreach ($order->products as $product) {
            $emails[] = Vendor::find($product->vendor_id)->email;
        }

        $price = 0;
        $products = [];
        foreach ($order->products as $product) {
            $price += $product->price;
            $products[] = $product->name;
        }

        foreach (array_unique($emails) as $email) {
            Mail::to($email)->send(new MailOrderDone($id, $products, $price));
        }

        return true;
    }

    /**
     * @param int $orderId
     * @return OrderAR
     */
    public function getById(int $orderId): OrderAR
    {
        return OrderAR::find($orderId);
    }

    /**
     * @return OrderAR
     */
    public function getAll()
    {
        return OrderAR::orderBy('id', 'asc')->paginate(25);
    }

}