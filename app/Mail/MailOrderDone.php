<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailOrderDone extends Mailable
{
    use Queueable, SerializesModels;

    private $id;
    private $products;
    private $price;

    /**
     * Create a new message instance.
     *
     * @param int $id
     * @param array $products
     * @param string $price
     */
    public function __construct(int $id, array $products, string $price)
    {
        $this->id = $id;
        $this->products = $products;
        $this->price = $price;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('market@xyz.xyz', 'marketXYZ')
            ->subject('ORder Done')
            ->markdown('mails.orderdone')
            ->with([
                'id' => $this->id,
                'products' => $this->products,
                'price' => $this->price
            ]);
    }
}
