<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Partner;
use App\Models\OrderProduct;

class Order extends Model
{
    protected $table = 'orders';

    /**
     * @return HasOne
     */
    public function partner()
    {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }

    public function orderProducts()
    {
        return $this->hasMany('OrderProduct', 'id', 'order_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class , 'order_products', 'order_id', 'product_id');
    }

}
