<?php

namespace App\Exceptions;

/**
 * Class ErrorStoreDBException
 */
class ErrorGetWeatherException extends \Exception
{
    public function __construct(string $message = '', int $code = 1, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}