<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Weather\OpenWeather;
use App\Services\Interfaces\WeatherInterface;

class WeatherServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WeatherInterface::class, function () {
            return new OpenWeather(config('app.openweather_key'));
        });
    }
}
