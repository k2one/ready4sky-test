<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Order;
use App\Services\Interfaces\OrderInterface;

class OrderServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrderInterface::class, function () {
            return new Order();
        });
    }
}
