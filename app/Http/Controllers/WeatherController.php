<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class WeatherController
 */
class WeatherController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('weather.index', [
            'privet' => 'Загружаем текущую погоду в славном городе Брянске ...'
        ]);
    }

}
