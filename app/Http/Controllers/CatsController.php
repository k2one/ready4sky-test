<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\View\View;
use App\Services\Interfaces\OrderInterface;

/**
 * Class WeatherController
 */
class CatsController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @param OrderInterface $orderInterface
     * @return View
     */
    public function index(OrderInterface $orderInterface): View
    {
        return view('cats.index', [
            'orders' =>  $orders = $orderInterface->getAll(),
        ]);
    }

    /**
     * @param OrderInterface $orderInterface
     * @param int $orderId
     * @return View
     */
    public function show(OrderInterface $orderInterface, int $orderId): View
    {
        return view('cats.show', [
            'order'     => $orderInterface->getById($orderId),
            'partners'  => Partner::get()
        ]);
    }

}
