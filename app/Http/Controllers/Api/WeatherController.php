<?php

namespace App\Http\Controllers\Api;

use \Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\WeatherInterface as Weather;
use App\Exceptions\ErrorGetWeatherException;

/**
 * Class WeatherController
 */
class WeatherController extends Controller
{

    private $weather;

    public function __construct(Weather $weather)
    {
        //$this->middleware('auth');
        $this->weather = $weather;
    }

    /**
     * @param string $city
     * @return JsonResponse
     */
    public function show(string $city): JsonResponse
    {
        try {
            $res = $this->weather->currentByCity($city);
        } catch (ErrorGetWeatherException $e) {
            return response()->json([], 500);
        }

        return response()->json($res);
    }

}
