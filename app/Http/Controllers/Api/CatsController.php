<?php

namespace App\Http\Controllers\Api;

use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\OrderInterface;

/**
 * Class WeatherController
 */
class CatsController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @param OrderInterface $orderInterface
     * @param Request $request
     * @param int $id
     * @return string
     */
    public function update(OrderInterface $orderInterface, Request $request, int $id): string
    {
        $orderInterface->store($id, $request->all());
        return 'true';
    }

}
