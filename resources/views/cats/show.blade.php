<div>
    Заказ {{ $order->id }}
</div>


<div>
    email клиента
    <a href="javascript:void(0)" id=clientEmailCur
       onclick="clientEmail_showForm('{{ $order->id }}', '{{ $order->client_email }}')">{{ $order->client_email }}</a>
    <div id="clientEmail_info"></div>
</div>


<div>
    партнер
    <select class="custom-select" id=Partner_Forms_select onchange="Partner_store('{{ $order->id }}')">
        @foreach($partners as $partner)
            <option value="{{ $partner->id }}"
                    @if($partner->id == $order->partner->id)
                    selected
                    @endif
            >{{ $partner->name }}</option>
        @endforeach
    </select>
    <div id="partnerList_info"></div>
</div>


<div>
    продукты

    <div>всего <?=count($order->products)?></div>

    <?php

    $prod = [];
    foreach ($order->products as $product) {

        if (empty($prod[$product->name])) {
            $prod[$product->name] = 1;
        } else {
            $prod[$product->name]++;
        }
    }?>

    @foreach($prod as $name => $qty)
        <div>{{ $name }} - {{$qty}} шт.</div>
    @endforeach

</div>


<div>
    статус заказа

    <select class="custom-select" id=status_Forms_select onchange="status_store('{{ $order->id }}')">
        <option value="0"
                @if($order->status === 0)
                selected
                @endif
        >новый
        </option>
        <option value="10"
                @if($order->status === 0)
                selected
                @endif
        >подтвержден
        </option>
        <option value="20"
                @if($order->status === 0)
                selected
                @endif
        >завершен
        </option>
    </select>

    <div id="status_info"></div>
</div>


<div>
    цена заказа
    <?php
    $price = 0;
    foreach ($order->products as $product) {
        $price += $product->price;
    }
    echo $price;
    ?>
</div>


<script>
    function clientEmail_showForm(orderId, clientEmail) {
        $('#clientEmailCur').hide();
        const div_val = $('#clientEmail_info');

        div_val.html('<input type="text" id=emailClientForm value="' + clientEmail + '" class="form-control" autocomplete="off" required ' +
            'autofocus onkeypress="clientEmail_keyEvent(event, \'' + orderId + '\')"' +
            'onBlur="clientEmail_Store(\'' + orderId + '\')"' +
            ' style="width: 400px;">');
    }

    function clientEmail_keyEvent(e, orderId) {
        if (e.keyCode === 13) {
            clientEmail_Store(orderId);
        }
    }

    function clientEmail_Store(orderId) {
        let new_value = $('#emailClientForm').val();
        let div_msg = $('#clientEmail_info');

        if (new_value == '' || new_value == undefined) {
            div_msg.append(' error');
        } else {
            $.ajax({
                type: "PUT",
                url: "/api/cats/" + orderId,
                dataType: 'json',
                data: {
                    _token: '<?=csrf_token() ?>',
                    client_email: new_value,
                },
                success: function (data) {
                    if (data) {
                        $('#clientEmailCur').html(new_value).show();
                        div_msg.hide();
                    } else {
                        div_msg.html('error');
                    }
                },
                error: function (data) {
                    div_msg.html('error');
                }
            });
        }
    }

    function Partner_store(orderId) {
        let new_value = $('#Partner_Forms_select').val();
        let div_msg = $('#partnerList_info');

        $.ajax({
            type: "PUT",
            url: "/api/cats/" + orderId,
            dataType: 'json',
            data: {
                _token: '<?=csrf_token() ?>',
                partner_id: new_value,
            },
            success: function (data) {
                if (data) {
                    $('#clientEmailCur').html(new_value).show();
                    div_msg.html('done');
                } else {
                    div_msg.html('error');
                }
            },
            error: function (data) {
                div_msg.html('error');
            }
        });
    }

    function status_store(orderId) {
        let new_value = $('#status_Forms_select').val();
        let div_msg = $('#status_info');

        $.ajax({
            type: "PUT",
            url: "/api/cats/" + orderId,
            dataType: 'json',
            data: {
                _token: '<?=csrf_token() ?>',
                status: new_value,
            },
            success: function (data) {
                if (data) {
                    $('#clientEmailCur').html(new_value).show();
                    div_msg.html('done');
                } else {
                    div_msg.html('error');
                }
            },
            error: function (data) {
                div_msg.html('error');
            }
        });
    }
</script>


<?php

/*{{ Form::open(array('url' => '/cats/'.$order->id, 'method' => 'put')) }}

    <div>email клиента {{ Form::text('client_email', $order->client_email, array('required' => 'required')) }}</div>



    {{ Form::submit('save') }}

{{ Form::close() }}

*/?>


