@extends('welcome')

@section('content')

    {{ $orders->links() }}

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">ид_заказа</th>
            <th scope="col">название_партнера</th>
            <th scope="col">стоимость_заказа</th>
            <th scope="col">наименование_состав_заказа</th>
            <th scope="col">статус_заказа</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($orders as $order)
            <tr>
                <?php /*<th scope="row"><a href="/cat/{{ $order->id }}">{{ $order->id }}</a></th>*/?>
                <th scope="row"><a href="javascript:void(0)" onclick="showOrder({{ $order->id }})">{{ $order->id }}</a></th>
                <td>{{ $order->partner->name }}</td>
                <td>
                    <?php
                    $price = 0;
                    foreach ($order->products as $product) {
                        $price += $product->price;
                    }
                    echo $price;
                    ?>
                </td>
                <td>
                    @foreach ($order->products as $product)
                        <div>{{ $product->name }}</div>
                    @endforeach
                </td>
                <td>
                    @if($order->status === 0)
                        новый
                    @elseif($order->status === 10)
                        подтвержден
                    @elseif($order->status === 20)
                        завершен
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
