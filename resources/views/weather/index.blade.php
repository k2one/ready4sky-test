@extends('welcome')

@section('content')

    <div id="weather_hi">{{$privet}}</div>

    <div id="weather_there" style="display: none;">
        <div>Температура: <span id="temp"></span></div>
        <div>Описание: <span id="description"></span></div>
        <div>Атмосферное давление: <span id="pressure"></span></div>
        <div>Влажность: <span id="humidity"></span></div>
        <div>Скорость ветра: <span id="wind_speed"></span></div>
        <div>Максимальная зафиксированная температура: <span id="temp_min"></span></div>
        <div>Минимальная зафиксированная температура: <span id="temp_max"></span></div>
    </div>

    <script>
        window.onload = function () {
            getWeatherByCity('Bryansk');
        }
    </script>

@endsection
