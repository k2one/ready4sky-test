@component('mail::message')
    Привет.
    Заказ {{ $id }} успешно выполнен.
    Состав заказа
    @foreach($products as $product)
        <div>{{ $product }}</div>
    @endforeach

    Общая сумма заказа {{ $price }}
@endcomponent