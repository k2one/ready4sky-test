function getWeatherByCity(city) {
    const div_info = $('#weather_there');
    const div_hi = $('#weather_hi');

    $.ajax({
        type: "GET",
        url: "/api/weather/" + city,
        dataType: 'json',
        success: function (res) {console.log(res);
            div_hi.hide();
            div_info.show();
            $('#temp').html(res.temp);
            $('#description').html(res.description);
            $('#pressure').html(res.pressure);
            $('#humidity').html(res.humidity);
            $('#wind_speed').html(res.wind_speed);
            $('#temp_min').html(res.temp_min);
            $('#temp_max').html(res.temp_max);
        },
        error: function (res) {console.log(res);
            div_hi.html('Возникла ошибка');
        }
    });
}

function showOrder(id) {
    console.log(id);
    $("#orderOne").modal({
        backdrop: 'static',
        keyboard: false
    });

    //let div_projectOne_content = $('#projectOne_content');
    //loader_circle(div_projectOne_content);

    $.ajax({
        type: "GET",
        url: "/cats/"+id,
        datatype: 'html',
        success: function (data) {
            $("#orderOne_content").html(data);
            //console.log(xhr + "\n" + textStatus + "\n" + thrownError);
        },
        error: function (xhr, textStatus, thrownError) {
            console.log(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });

    //window.location.href = "/projects/"+uuid+"/html";
}