<?php

namespace Tests\Unit;

use App\Services\Interfaces\WeatherInterface as Weather;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WeatherTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetByCity()
    {
        $container = app();
        $weather = $container->make(Weather::class);
        $res = $weather->currentByCity('Bryansk');
        //$this->assertIsArray($res);
        $this->assertNotEmpty($res);
    }
}
