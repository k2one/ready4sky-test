<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/weather', ['as' => 'weather_current', 'uses' => 'WeatherController@index']);

Route::get('/cats', ['as' => 'cats', 'uses' => 'CatsController@index']);
Route::get('/cats/{id}', ['as' => 'cat', 'uses' => 'CatsController@show']);

